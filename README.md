# CNRGH ELK Ansible Collection

# Installation

Create a file named ``requirements.yml`` at the root of your project and include this collection as follows:
```yml
---
collections:
  - name: git+https://gitlab.com/cnrgh/cnrgh.elk.git
```

Then, install the requirements by running ``ansible-galaxy install -r requirements.yml``.

You can now import the roles included in this collection in your own playbooks:

```yml
- name: Roles demo
  hosts: all
  collections: # Import the collection.
    - cnrgh.elk
  roles: # Use the roles included in the collection.
    debug
    elasticsearch
    kibana
```

# Available roles

## Debug role

A role for configuring a machine for debugging inside the machine.

## Elasticsearch role

A for installing [Elasticsearch](https://www.elastic.co/) 
on distributions based on [RHEL](https://fr.wikipedia.org/wiki/Red_Hat_Enterprise_Linux).

**Defining roles**

Create a YAML file `roles.yml`.

Example for creating a role to access one single index through Kibana:
```yaml
---
- name: role1
  desc:
    indices:
      - names: ['myindex']
        privileges: ['all']
    applications:
      - application: "kibana-.kibana*"
        privileges: ["*"]
        resources: ["*"]
```

And set the path to this file into `es_roles_file` variable.

**Defining users**

Create a YAML file `users.yml`:
```yaml
---
- name: user1
  profile:
    - password: mypassword
      roles: ["role1", "role2"]
      full_name: User Name
      email: user@some.site
```

And set the path to this file into `es_users_file` variable.

## Kibana role

A for installing [Kibana](https://www.elastic.co/kibana) 
on distributions based on [RHEL](https://fr.wikipedia.org/wiki/Red_Hat_Enterprise_Linux).
